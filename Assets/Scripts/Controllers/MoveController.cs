using Components;
using GameElements;
using UnityEngine;

public class MoveController : MonoBehaviour, IGameInitElement, IGameStartElement, IGameFinishElement
{
    private KeyboardInput _input;
    private IComponent_MoveInDirection _characterMoveComponent;

    private void OnMove(Vector3 direction)
    {
        _characterMoveComponent.Move(direction);
    }

    public void InitGame(IGameContext context)
    {
        _input = context.GetService<KeyboardInput>();
        _characterMoveComponent = context.GetService<CharacterService>()
            .GetCharacter().Get<IComponent_MoveInDirection>();
    }

    public void StartGame(IGameContext context)
    {
        _input.OnMove += OnMove;
    }

    public void FinishGame(IGameContext context)
    {
        _input.OnMove -= OnMove;
    }
}
