﻿using Components;
using GameElements;
using UnityEngine;

public class FallingCharacterController : MonoBehaviour, IGameInitElement, IGameStartElement, IGameFinishElement

{
    [SerializeField] private int _minHeight;
    
    private Transform _characterTransform;
    private GameElements.GameContext _context;

    // public void Construct(GameContext context)
    // {
    //     _characterTransform = context.GetService<CharacterService>()
    //         .GetCharacter().Get<IComponent_Transform>().CharacterTransform(); 
    //     _context = context;
    // }

    private void Awake()
    {
        enabled = false;
    }

    // void IStartGameListener.OnStartGame()
    // {
    //     enabled = true;
    // }
    //
    // void IFinishGameListener.OnFinishGame()
    // {
    //     enabled = false;
    // }

    private void Update()
    {
        if (_characterTransform.position.y < _minHeight)
            FinishGame();
    }

    private void FinishGame()
    {
        _context.FinishGame();
    }

    public void InitGame(IGameContext context)
    {
        _characterTransform = context.GetService<CharacterService>()
            .GetCharacter().Get<IComponent_Transform>().CharacterTransform(); 
        _context = (GameElements.GameContext) context;
    }

    public void StartGame(IGameContext context)
    {
        enabled = true;
    }

    public void FinishGame(IGameContext context)
    {
        enabled = false;
    }
}
