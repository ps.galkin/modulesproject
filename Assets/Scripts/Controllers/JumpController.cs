using Components;
using GameElements;
using UnityEngine;

public class JumpController : MonoBehaviour, IGameInitElement, IGameStartElement, IGameFinishElement
{
    private KeyboardInput _input;
    private IComponent_Jump _characterJumpComponent;
    
    public void InitGame(IGameContext context)
    {
        _input = context.GetService<KeyboardInput>();
        _characterJumpComponent = context.GetService<CharacterService>()
            .GetCharacter().Get<IComponent_Jump>();
    }

    public void StartGame(IGameContext context)
    {
        _input.OnJump += OnJump;
    }

    public void FinishGame(IGameContext context)
    {
        _input.OnJump -= OnJump;
    }
    
    private void OnJump()
    { 
        _characterJumpComponent.Jump();
    }
}
