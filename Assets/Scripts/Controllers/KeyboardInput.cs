using System;
using GameElements;
using UnityEngine;

public class KeyboardInput : MonoBehaviour, IGameStartElement, IGameFinishElement
{
    public event Action<Vector3> OnMove;
    public event Action OnJump;
    public event Action OnAttack;

    private void Awake()
    {
        enabled = false;
    }
    
    private void Update()
    {
        HandleKeyboard();
    }
    
    private void HandleKeyboard()
    {
        if (Input.GetKey(KeyCode.UpArrow))
            Move(Vector3.forward);
        else if (Input.GetKey(KeyCode.DownArrow))
            Move(Vector3.back);
        else if (Input.GetKey(KeyCode.LeftArrow))
            Move(Vector3.left);
        else if (Input.GetKey(KeyCode.RightArrow))
            Move(Vector3.right);
        else
            Move(Vector3.zero);
        
        if (Input.GetKey(KeyCode.Space))
            Jump();
        
        if (Input.GetMouseButton(0))
            Attack();
    }

    private void Move(Vector3 direction)
    {
        OnMove?.Invoke(direction);
    }

    private void Jump()
    {
        OnJump?.Invoke();
    }

    private void Attack()
    {
        OnAttack?.Invoke();
    }

    public void StartGame(IGameContext context)
    {
        enabled = true;
    }

    public void FinishGame(IGameContext context)
    {
        enabled = false;
    }
}

