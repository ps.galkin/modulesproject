using Components;
using GameElements;
using UnityEngine;

public class AttackController : MonoBehaviour, IGameInitElement, IGameStartElement, IGameFinishElement
{
    private KeyboardInput _input;
    private IComponent_Attack _characterAttackComponent;
    
    public void InitGame(IGameContext context)
    {
        _input = context.GetService<KeyboardInput>();
        _characterAttackComponent = context.GetService<CharacterService>()
            .GetCharacter().Get<IComponent_Attack>();
    }

    public void StartGame(IGameContext context)
    {
        _input.OnAttack += OnAttack;
    }

    public void FinishGame(IGameContext context)
    {
        _input.OnAttack -= OnAttack;
    }
    private void OnAttack()
    {
        _characterAttackComponent.Attack();
    }
}
