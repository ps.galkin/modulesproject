﻿namespace Components
{
    public interface IComponent_HitPoints
    {
        void SetHitPoints(int hitPoints);
        int GetHitPoints();
    }
}