﻿using UnityEngine;

namespace Components
{
    public class Component_MeleeDamage : MonoBehaviour, IComponent_MeleeDamage
    {
        [SerializeField] private IntBehaviour _meleeDamage;
        
        public void SetMeleeDamage(int damage)
        {
            _meleeDamage.Value = damage;
        }

        public int GetMeleeDamage()
        {
            return _meleeDamage.Value;
        }
    }
}