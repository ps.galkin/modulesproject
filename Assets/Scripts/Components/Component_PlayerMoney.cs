﻿using CoreMechanics.Mechanics;
using UnityEngine;

namespace Components
{
    public class Component_PlayerMoney : MonoBehaviour, IComponent_PlayerMoney
    {
        [SerializeField] private PlayerMoneyEngine _playerMoneyEngine;
        
        public void SetMoneyCount(int moneyCount)
        {
            _playerMoneyEngine.SetMoney(moneyCount);
        }

        public int GetMoneyCount()
        {
            return _playerMoneyEngine.GetMoney();
        }

        public void AddMoney(int moneyCount)
        {
            _playerMoneyEngine.AddMoney(moneyCount);
        }

        public void SpendMoney(int moneyCount)
        {
            _playerMoneyEngine.SpendMoney(moneyCount);
        }
    }
}