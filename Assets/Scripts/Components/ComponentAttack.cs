﻿using UnityEngine;

namespace Components
{
    public class ComponentAttack : MonoBehaviour, IComponent_Attack
    {
        [SerializeField] private EventReceiver _attackReceiver;
    
        public void Attack()
        {
            _attackReceiver.Call();
        }
    }
}