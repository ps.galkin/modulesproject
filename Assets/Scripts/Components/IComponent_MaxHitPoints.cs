﻿namespace Components
{
    public interface IComponent_MaxHitPoints
    {
        void SetMaxHitPoints(int hitPoints);
        int GetMaxHitPoints();
    }
}