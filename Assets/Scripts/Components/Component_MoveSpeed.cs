﻿using UnityEngine;

namespace Components
{
    public class Component_MoveSpeed : MonoBehaviour, IComponent_MoveSpeed
    {
        [SerializeField] private IntBehaviour _moveSpeed;
        
        public void SetMoveSpeed(int speed)
        {
            _moveSpeed.Value = speed;
        }

        public int GetMoveSpeed()
        {
            return _moveSpeed.Value;
        }
    }
}