﻿namespace Components
{
    public interface IComponent_MeleeDamage
    {
        void SetMeleeDamage(int damage);
        int GetMeleeDamage();
    }
}