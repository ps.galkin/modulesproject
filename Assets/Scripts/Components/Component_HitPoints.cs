﻿using UnityEngine;

namespace Components
{
    public class Component_HitPoints : MonoBehaviour, IComponent_MaxHitPoints, IComponent_HitPoints
    {
        [SerializeField] private HitPointsEngine _engine;
    
        public void SetMaxHitPoints(int hitPoints)
        {
            _engine.MaxHitPoints = hitPoints;
        }

        public int GetMaxHitPoints()
        {
            return _engine.MaxHitPoints;
        }

        public void SetHitPoints(int hitPoints)
        {
            _engine.CurrentHitPoints = hitPoints;
        }

        public int GetHitPoints()
        {
            return _engine.CurrentHitPoints;
        }
    }
}