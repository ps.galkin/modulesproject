﻿using UnityEngine;

namespace Components
{
    public interface IComponent_Transform
    {
        Transform CharacterTransform();
    }
}