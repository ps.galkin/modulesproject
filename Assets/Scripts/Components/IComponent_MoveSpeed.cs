﻿namespace Components
{
    public interface IComponent_MoveSpeed
    {
        void SetMoveSpeed(int speed);
        int GetMoveSpeed();
    }
}