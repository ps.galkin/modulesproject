﻿using UnityEngine;

namespace Components
{
    public class ComponentJump : MonoBehaviour, IComponent_Jump
    {
        [SerializeField] private EventReceiver _jumpReceiver;
    
        public void Jump()
        {
            _jumpReceiver.Call();
        }
    }
}