﻿using UnityEngine;

namespace Components
{
     public class ComponentTransform : MonoBehaviour, IComponent_Transform
     {
          [SerializeField] private TransformBehaviour _characterTransform;

          public Transform CharacterTransform()
          {
               return _characterTransform.Value;
          }
     }
}