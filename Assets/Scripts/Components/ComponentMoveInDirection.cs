using UnityEngine;

namespace Components
{
    public class ComponentMoveInDirection : MonoBehaviour, IComponent_MoveInDirection
    {
        [SerializeField] private VectorEventReceiver _moveReceiver;
    
        public void Move(Vector3 direction)
        {
            _moveReceiver.Call(direction);
        }
    }
}