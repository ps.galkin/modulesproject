﻿namespace Components
{
    public interface IComponent_PlayerMoney
    {
        void SetMoneyCount(int count);
        int GetMoneyCount();
        void AddMoney(int count);
        void SpendMoney(int count);
    }
}