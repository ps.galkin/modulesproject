﻿using GameElements;

public interface IGameDataLoader
{
    void LoadData(IGameContext context);
}
