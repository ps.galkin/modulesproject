using System.Collections;
using GameElements.Unity;
using Services;
using Services.Unity;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

public class ApplicationManager : MonoBehaviour
{
    [SerializeField] private ServiceInstaller _serviceInstaller;

    private bool _applicationLoaded;

    private MonoGameContext _gameContext;

    [Button]
    public void LoadApplication()
    {
        if (!_applicationLoaded)
        {
            StartCoroutine(LoadRoutine());
        }
    }

    private IEnumerator LoadRoutine()
    {
        InstallServices();
        yield return LoadGameScene();
        LoadGameData();
        StartGame();
        _applicationLoaded = true;
    }

    private void InstallServices()
    {
        _serviceInstaller.InstallServices();
        ServiceInjector.ResolveDependencies();
    }

    private IEnumerator LoadGameScene()
    {
        const string sceneId = "Assets/Scenes/GameScene.unity";
        var operation = Addressables.LoadSceneAsync(sceneId, LoadSceneMode.Additive);
        yield return operation;

        _gameContext = FindObjectOfType<MonoGameContext>();
        _gameContext.LoadGame();
    }

    private void LoadGameData()
    {
        var dataLoaders = ServiceLocator.GetServices<IGameDataLoader>();
        foreach (var dataLoader in dataLoaders)
        {
            dataLoader.LoadData(_gameContext);
        }
    }

    private void StartGame()
    {
        _gameContext.InitGame();
        _gameContext.ReadyGame();
        _gameContext.StartGame();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            SaveGameData();
        }
    }

    private void OnApplicationQuit()
    {
        SaveGameData();
    }

    private void SaveGameData()
    {
        if (!_applicationLoaded)
        {
            return;
        }

        var dataSavers = ServiceLocator.GetServices<IGameDataSaver>();
        foreach (var dataSaver in dataSavers)
        {
            dataSaver.SaveData(_gameContext);
        }
    }
}
