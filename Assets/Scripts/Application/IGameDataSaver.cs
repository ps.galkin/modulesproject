﻿using GameElements;

public interface IGameDataSaver
{
    void SaveData(IGameContext context);
}
