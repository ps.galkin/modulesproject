using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private IntEventReceiver _takeDamageReceiver;
    [SerializeField] private TransformBehaviour _transform;

    public Transform Transform => _transform.Value; 
    
    public void TakeDamage(int damage)
    {
        _takeDamageReceiver.Call(damage);
    }

}
