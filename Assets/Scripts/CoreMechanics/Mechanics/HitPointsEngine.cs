﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

public class HitPointsEngine : MonoBehaviour
{
    public event Action OnSetuped;

    public event Action<int> OnHitPointsChanged;

    public event Action<int> OnMaxHitPointsChanged;

    public event Action OnHitPointsFull;

    public event Action OnHitPointsEmpty;

    public int CurrentHitPoints
    {
        get => _currentHitPoints;
        set => SetHitPoints(value);
    }

    public int MaxHitPoints
    {
        get => _maxHitPoints;
        set => SetMaxHitPoints(value);
    }

    [SerializeField] private int _maxHitPoints;

    [SerializeField] private int _currentHitPoints;

    [Title("Methods")]
    [GUIColor(0, 1, 0)]
    [Button]
    public void Setup(int current, int max)
    {
        _maxHitPoints = max;
        _currentHitPoints = Mathf.Clamp(current, 0, _maxHitPoints);
        OnSetuped?.Invoke();
    }

    [GUIColor(0, 1, 0)]
    [Button]
    private void SetHitPoints(int value)
    {
        value = Mathf.Clamp(value, 0, _maxHitPoints);
        _currentHitPoints = value;
        OnHitPointsChanged?.Invoke(_currentHitPoints);

        if (value <= 0)
        {
            OnHitPointsEmpty?.Invoke();
        }

        if (value >= _maxHitPoints)
        {
            OnHitPointsFull?.Invoke();
        }
    }

    [Button]
    [GUIColor(0, 1, 0)]
    private void SetMaxHitPoints(int value)
    {
        value = Math.Max(1, value);
        if (_currentHitPoints > value)
        {
            _currentHitPoints = value;
        }

        _maxHitPoints = value;
        OnMaxHitPointsChanged?.Invoke(value);
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        _maxHitPoints = Math.Max(1, _maxHitPoints);
        _currentHitPoints = Mathf.Clamp(_currentHitPoints, 1, _maxHitPoints);
    }
#endif
}