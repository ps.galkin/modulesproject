using System.Collections;
using UnityEngine;

public sealed class BulletEngine : MonoBehaviour
{
    [SerializeField] private TransformBehaviour _playerTransform;
    
    private Bullet _bullet;
    private GameObject _bulletPrefab;

    private void Awake()
    {
        _bulletPrefab = Resources.Load<GameObject>("Bullet");
    }

    public void StartShoot(Vector3 targetVector)
    {
        // создаем пулю из ресурсов, 
        var bulletObj = Instantiate(_bulletPrefab);
        bulletObj.transform.position = _playerTransform.Value.position;
        
        // запускаем ее в нужном направлении. без корутины не работает. кажется, не успевают проинициализироваться объект 
        _bullet = bulletObj.GetComponent<Bullet>();
        StartCoroutine(TimerRoutine(targetVector));
    }

    private IEnumerator TimerRoutine(Vector3 targetVector)
    {
        yield return new WaitForEndOfFrame();

        _bullet.Move(targetVector);
    }
}
