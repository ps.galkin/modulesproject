using UnityEngine;

public class BulletShootMechanics : MonoBehaviour
{
    [SerializeField] private VectorEventReceiver _shootReceiver;
    [SerializeField] private TimerBehaviour _rechargeTimer;
    [SerializeField] private BulletEngine _bulletEngine;
    
    private void OnEnable()
    {
        _shootReceiver.OnEvent += StartShoot;
    }
    
    private void OnDisable()
    {
        _shootReceiver.OnEvent -= StartShoot;
    }

    private void StartShoot(Vector3 targetVector)
    {
        if (_rechargeTimer.IsPlaying)
            return;
        
        _bulletEngine.StartShoot(targetVector);
    }
}
