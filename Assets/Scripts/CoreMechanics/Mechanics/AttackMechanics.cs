using UnityEngine;

public sealed class AttackMechanics : MonoBehaviour
{
    [SerializeField] private EventReceiver _attackReceiver;
    [SerializeField] private TimerBehaviour _countdown;
    [Space]
    [SerializeField] private Enemy _enemy;
    [SerializeField] private IntBehaviour _damage;
    [SerializeField] private IntBehaviour _meleeDamage;
    [SerializeField] private VectorEventReceiver _makeBulletReceiver;
    [SerializeField] private TransformBehaviour _playerTransform;
    [SerializeField] private BoolBehaviour _canAttack;
    
    private void OnEnable()
    {
        _attackReceiver.OnEvent += OnRequestAttack;
    }

    private void Start()
    {
        _canAttack.Value = true;
    }

    private void OnDisable()
    {
        _attackReceiver.OnEvent -= OnRequestAttack;
    }
    
    private void OnRequestAttack()
    {
        if (!_canAttack.Value)
            return;
        
        if (_countdown.IsPlaying)
            return;

        // враг получает урон
        _enemy.TakeDamage(_damage.Value);
        
        // запускаем пулю во врага
        _makeBulletReceiver.Call(_enemy.Transform.position - _playerTransform.Value.position);
        
        _countdown.ResetTime();
        _countdown.Play();
    }
}
