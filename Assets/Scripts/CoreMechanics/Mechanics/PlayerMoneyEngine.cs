﻿using UnityEngine;

namespace CoreMechanics.Mechanics
{
    public class PlayerMoneyEngine : MonoBehaviour
    {
        [SerializeField] private IntBehaviour _moneyCount;

        public void SetMoney(int moneyCount)
        {
            _moneyCount.Value = moneyCount;
        }
        
        public int GetMoney()
        {
            return _moneyCount.Value;
        }

        public void AddMoney(int moneyCount)
        {
            _moneyCount.Value += moneyCount;
        }

        public void SpendMoney(int moneyCount)
        {
            _moneyCount.Value -= moneyCount;
        }
    }
}