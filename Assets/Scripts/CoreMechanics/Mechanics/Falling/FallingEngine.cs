﻿using UnityEngine;

namespace CoreMechanics.Mechanics.Falling
{
    public class FallingEngine : MonoBehaviour
    {
        [SerializeField] private TransformBehaviour _playerTransform;
        [SerializeField] private IntBehaviour _fallingHeight;
        [SerializeField] private EventReceiver _fallingEvent;

        private void FixedUpdate()
        {
            if (_playerTransform.Value.position.y < _fallingHeight.Value)
                _fallingEvent.Call();
        }
    }
}