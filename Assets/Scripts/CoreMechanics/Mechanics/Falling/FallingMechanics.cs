using GameElements;
using UnityEngine;

public class FallingMechanics : MonoBehaviour
{
    private GameContext _gameContext;
    [SerializeField] private EventReceiver _fallingReceiver;

    private void OnEnable()
    {
        _fallingReceiver.OnEvent += _gameContext.FinishGame;
    }
    
    private void OnDisable()
    {
        _fallingReceiver.OnEvent -= _gameContext.FinishGame;
    }
}
