using System.Linq;
using UnityEngine;

public sealed class MoveMechanics : MonoBehaviour
{
    [SerializeField] private VectorEventReceiver _moveReceiver;
    [SerializeField] private MoveCondition[] _canMoveConditions;
    [SerializeField] private MoveEngine _moveEngine;

    private float _moveSpeedDeltaTime;

    private void OnEnable()
    {
        _moveReceiver.OnEvent += Move;
    }
    
    private void OnDisable()
    {
        _moveReceiver.OnEvent -= Move;
    }

    private void Move(Vector3 moveVector)
    {
        if (_canMoveConditions.All(cond => cond.CanMove(moveVector)))
        {
            _moveEngine.MoveVector = moveVector.normalized;
        }
    }
}

