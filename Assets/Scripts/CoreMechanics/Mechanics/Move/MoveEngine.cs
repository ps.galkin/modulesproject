using UnityEngine;

public sealed class MoveEngine : MonoBehaviour
{
    [SerializeField] private IntBehaviour _moveSpeed;
    [SerializeField] private TransformBehaviour _playerTransform;
    
    private Vector3 _moveVector;

    public Vector3 MoveVector
    {
        get => _moveVector;
        set => _moveVector = new Vector3(value.x, 0, value.normalized.z);
    }

    private void FixedUpdate()
    {
        _playerTransform.Value.transform.Translate(_moveVector * (_moveSpeed.Value * Time.fixedDeltaTime));
    }
}
