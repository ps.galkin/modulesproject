using UnityEngine;

public abstract class MoveCondition : MonoBehaviour
{
    public abstract bool CanMove(Vector3 vector);
}
