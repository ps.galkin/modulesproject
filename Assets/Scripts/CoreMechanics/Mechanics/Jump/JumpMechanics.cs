using UnityEngine;

public sealed class JumpMechanics : MonoBehaviour
{
    [SerializeField] private EventReceiver _jumpReceiver;
    [SerializeField] private JumpEngine _jumpEngine;
    
    private void OnEnable()
    {
        _jumpReceiver.OnEvent += _jumpEngine.Jump;
    }
    
    private void OnDisable()
    {
        _jumpReceiver.OnEvent -= _jumpEngine.Jump;
    }
}
