using System.Collections;
using UnityEngine;

public sealed class JumpEngine : MonoBehaviour
{
    [SerializeField] private TransformBehaviour _playerTransformBehaviour;
    [SerializeField] private IntBehaviour _jumpHeight;
    [SerializeField] private IntBehaviour _jumpSpeed;
    
    [SerializeField] private ActionBehaviour[] _beforeActions;
    [SerializeField] private ActionBehaviour[] _afterActions;
    
    private Coroutine _jumpCoroutine;
    private Transform _playerTransform;
    private Vector3 _upVector;
    private Vector3 _downVector;
     private void Start()
    {  
        _playerTransform = _playerTransformBehaviour.Value;
        
        _upVector = Vector3.up * (_jumpSpeed.Value * Time.fixedDeltaTime);
        _downVector = Vector3.down * (_jumpSpeed.Value * Time.fixedDeltaTime);
    }
     
     public void Jump()
     {
         if (_jumpCoroutine == null)
             _jumpCoroutine = StartCoroutine(JumpRoutine());
     }
    
    private IEnumerator JumpRoutine()
    {
        var startHeight = _playerTransformBehaviour.Value.position.y;

        foreach (var action in _beforeActions)
            action.Do();
        
        // прыжки сделал в виде корутины
        // двигаемся вверх
        while (_playerTransform.position.y <= _jumpHeight.Value)
        {
            _playerTransform.Translate(_upVector);
            
            yield return new WaitForFixedUpdate();
        }

        // и вниз
        while (_playerTransform.position.y >= startHeight)
        {
            _playerTransform.Translate(_downVector);
            
            yield return new WaitForFixedUpdate();
        }
        
        // после на всякий случай восстанавилваем высоту
        var position = _playerTransform.position;
        position = new Vector3(position.x, startHeight, position.z);
        _playerTransform.position = position;

        foreach (var action in _afterActions)
            action.Do();
        
        _jumpCoroutine = null;
    }
}
