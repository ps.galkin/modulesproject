using System;
using System.Collections;
using UnityEngine;

public sealed class PeriodBehaviour : MonoBehaviour
{
    public event Action OnEvent;

    public bool IsPlaying => _coroutine != null;

    private Coroutine _coroutine;

    [SerializeField] private float _period = 1.0f;

    public void Play()
    {
        if (_coroutine == null)
            _coroutine = StartCoroutine(PeriodRoutine());
    }

    public void Stop()
    {
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
            _coroutine = null;
        }
    }

    private IEnumerator PeriodRoutine()
    {
        var period = new WaitForSeconds(_period);
        while (true)
        {
            yield return period;
            OnEvent?.Invoke();
        }
    }
}
