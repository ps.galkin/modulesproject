using UnityEngine;

public class TransformBehaviour : MonoBehaviour
{
    public Transform Value => _value;
    
    [SerializeField] private Transform _value;
}
