using UnityEngine;

public abstract class ActionBehaviour : MonoBehaviour
{
    public abstract void Do();
}
