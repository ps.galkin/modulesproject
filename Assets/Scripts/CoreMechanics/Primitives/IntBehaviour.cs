using System;
using UnityEngine;

public sealed class IntBehaviour : MonoBehaviour
{
    public event Action<int> OnValueChanged;
    
    public int Value
    {
        get => _value;
        set
        {
            _value = value; 
            OnValueChanged?.Invoke(value);
        }
    }
    
    [SerializeField] private int _value;
}
