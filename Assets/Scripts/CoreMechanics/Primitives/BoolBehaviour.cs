using System;
using UnityEngine;

public class BoolBehaviour : MonoBehaviour
{
    public event Action<bool> OnValueChanged;
    
    public bool Value
    {
        get => _value;
        set
        {
            _value = value; 
            OnValueChanged?.Invoke(value);
        }
    }
    
    [SerializeField] private bool _value;
}
