using System;
using Sirenix.OdinInspector;
using UnityEngine;

public class VectorEventReceiver : MonoBehaviour
{
    public event Action<Vector3> OnEvent;

    [Button]
    public void Call(Vector3 value)
    {
        OnEvent?.Invoke(value);
    }
}
