using System;
using Sirenix.OdinInspector;
using UnityEngine;

public class BoolEventReceiver : MonoBehaviour
{
    public event Action<bool> OnEvent;

    [Button]
    public void Call(bool value)
    {
        OnEvent?.Invoke(value);
    }
}
