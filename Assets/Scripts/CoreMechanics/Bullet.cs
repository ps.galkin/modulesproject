using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private VectorEventReceiver _bulletMove;
    
    public void Move(Vector3 direction)
    {
        _bulletMove.Call(direction);
    } 
}
