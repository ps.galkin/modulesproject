using System;
using Components;
using GameElements;
using Sirenix.OdinInspector;
using UnityEngine;

public sealed class MoneyStorage : MonoBehaviour, IGameInitElement
{
    public event Action<BigNumber> OnMoneyChanged;
    
    [ReadOnly] 
    [ShowInInspector] private BigNumber _money;
    
    private IComponent_PlayerMoney _playerMoneyComponent;
    public BigNumber Money => _money;
    
    public void InitGame(IGameContext context)
    {
        _playerMoneyComponent = context.GetService<CharacterService>()
            .GetCharacter().Get<IComponent_PlayerMoney>();
    }
    
    [Button]
    public void Add100Money()
    {
        var addedMoney = new BigNumber(100);
        _money += addedMoney;
        OnMoneyChanged?.Invoke(_money);
        _playerMoneyComponent.AddMoney(addedMoney.ToInt());
    }

    [Button]
    public void SpendMoney(BigNumber range)
    {
        _money -= range;
        OnMoneyChanged?.Invoke(_money);
        _playerMoneyComponent.SpendMoney(range.ToInt());
    }
}
