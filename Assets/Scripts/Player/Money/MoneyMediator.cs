﻿using Components;
using GameElements;
using Services;

namespace Player.Money
{
    public class MoneyMediator : IGameDataSaver, IGameDataLoader
    {
        private MoneyRepository _repository;

        [Inject]
        public void Construct(MoneyRepository repository)
        {
            _repository = repository;
        }
    
        public void LoadData(IGameContext context)
        {
            if (_repository.TryLoadStats(out MoneyData moneyData))
            {
                var character = context.GetService<CharacterService>().GetCharacter();
                character.Get<IComponent_PlayerMoney>().SetMoneyCount(moneyData.Count);
            }
        }

        public void SaveData(IGameContext context)
        {
            var character = context.GetService<CharacterService>().GetCharacter();
            var data = new MoneyData
            {
                Count = character.Get<IComponent_PlayerMoney>().GetMoneyCount(),
            };
        
            _repository.SaveStats(data);
        }
    }
}