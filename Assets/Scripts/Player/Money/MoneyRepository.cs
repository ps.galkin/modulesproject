﻿using UnityEngine;

namespace Player.Money
{
    public class MoneyRepository: MonoBehaviour
    {
        private const string MONEY_PREFS_KEY = "Lesson/MoneyData";

        public bool TryLoadStats(out MoneyData data)
        {
            if (PlayerPrefs.HasKey(MONEY_PREFS_KEY))
            {
                var json = PlayerPrefs.GetString(MONEY_PREFS_KEY);
                data = JsonUtility.FromJson<MoneyData>(json);
                
                Debug.Log($"<color=orange>LOAD MONEY DATA {json}</color>");
                return true;
            }

            data = default;
            return false;
        }

        public void SaveStats(MoneyData data)
        {
            var json = JsonUtility.ToJson(data);
            PlayerPrefs.SetString(MONEY_PREFS_KEY, json);

            Debug.Log($"<color=yellow>SAVE MONEY DATA {json}</color>");
        }
    }
}