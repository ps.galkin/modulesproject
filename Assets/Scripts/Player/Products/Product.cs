using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(
    fileName = "Product",
    menuName = "Lessons/New Product (Presentation Model)"
)]
public sealed class Product : ScriptableObject
{
    [PreviewField] [SerializeField] public Sprite Icon;

    [SerializeField] public string Title;

    [TextArea] [SerializeField] public string Description;
    
    [Space] [SerializeField] public BigNumber price;
}
