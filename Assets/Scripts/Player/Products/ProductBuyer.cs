using GameElements;
using Sirenix.OdinInspector;
using UnityEngine;

public sealed class ProductBuyer : MonoBehaviour, IGameInitElement
{
    private MoneyStorage _moneyStorage;

    [Button]
    public bool CanBuy(Product product)
    {
        return _moneyStorage.Money >= product.price;
    }

    [Button]
    public void Buy(Product product)
    {
        if (CanBuy(product))
        {
            _moneyStorage.SpendMoney(product.price);
            Debug.Log($"<color=green>Product {product.Title} successfully purchased!</color>");
        }
        else
        {
            Debug.LogWarning($"<color=red>Not enough money for product {product.Title}!</color>");
        }
    }

    public void InitGame(IGameContext context)
    {
        _moneyStorage = context.GetService<MoneyStorage>();
    }
}
