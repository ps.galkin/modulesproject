﻿using GameElements;
using Sirenix.OdinInspector;
using UnityEngine;

public class ProductShower : MonoBehaviour, IGameInitElement
{
    private PopupManager _popupManager;

    //private ProductPresentationModelFactory _presenterFactory;
    private ProductBuyer _productBuyer;
    private MoneyStorage _moneyStorage;

    [Button]
    public void ShowProduct(Product product)
    {
        // var presentationModel = _presenterFactory.CreatePresenter(product);
        var presentationModel = new ProductPresentationModel(product, _productBuyer, _moneyStorage);
        _popupManager.ShowPopup(PopupName.PRODUCT, presentationModel);
    }

    public void InitGame(IGameContext context)
    {
        _popupManager = context.GetService<PopupManager>();
        _productBuyer = context.GetService<ProductBuyer>();
        _moneyStorage = context.GetService<MoneyStorage>();
    }
}
