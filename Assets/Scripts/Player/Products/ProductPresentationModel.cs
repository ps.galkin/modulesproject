﻿using System;
using UnityEngine;

public class ProductPresentationModel : ProductPopup.IPresentationModel
{
    public event Action<bool> OnBuyButtonStateChanged;
        
    private readonly Product _product;

    private readonly ProductBuyer _productBuyer;
        
    private readonly MoneyStorage _moneyStorage;

    public ProductPresentationModel(Product product, ProductBuyer productBuyer, MoneyStorage moneyStorage)
    {
        _product = product;
        _productBuyer = productBuyer;
        _moneyStorage = moneyStorage;
    }
        
    public void Start()
    {
        _moneyStorage.OnMoneyChanged += OnMoneyChanged;
    }

    public void Stop()
    {
        _moneyStorage.OnMoneyChanged -= OnMoneyChanged;
    }

    public string GetTitle()
    {
        return _product.Title;
    }

    public string GetDescription()
    {
        return _product.Description;
    }

    public Sprite GetIcon()
    {
        return _product.Icon;
    }

    public string GetPrice()
    {
        return _product.price.ToString();
    }

    public bool CanBuy()
    {
        return _productBuyer.CanBuy(_product);
    }

    public void OnBuyClicked()
    {
        _productBuyer.Buy(_product);
    }

    private void OnMoneyChanged(BigNumber money)
    {
        var canBuy = _productBuyer.CanBuy(_product);
        OnBuyButtonStateChanged?.Invoke(canBuy);
    }
}
