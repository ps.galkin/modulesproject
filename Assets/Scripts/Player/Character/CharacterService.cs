﻿using Entities;
using UnityEngine;

public class CharacterService : MonoBehaviour
{
    [SerializeField] private UnityEntityBase _entity;

    public UnityEntityBase GetCharacter()
    {
        return _entity;
    }
}