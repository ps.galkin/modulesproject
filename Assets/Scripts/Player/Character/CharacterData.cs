using System;

[Serializable]
public struct CharacterData
{
     public int CurrentHipPoints;
     public int MaxHitPoints;
     public int MeleeDamage;
     public int MoveSpeed;
}