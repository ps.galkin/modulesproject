using Components;
using GameElements;
using Services;
using UnityEngine;

public class CharacterMediator : MonoBehaviour, IGameDataSaver, IGameDataLoader
{
    private CharacterRepository _repository;

    [Inject]
    public void Construct(CharacterRepository repository)
    {
        _repository = repository;
    }
    
    public void LoadData(IGameContext context)
    {
        if (_repository.TryLoadStats(out CharacterData data))
        {
            var character = context.GetService<CharacterService>().GetCharacter();
            character.Get<IComponent_MaxHitPoints>().SetMaxHitPoints(data.MaxHitPoints);
            character.Get<IComponent_HitPoints>().SetHitPoints(data.CurrentHipPoints);
            character.Get<IComponent_MeleeDamage>().SetMeleeDamage(data.MeleeDamage);
            character.Get<IComponent_MoveSpeed>().SetMoveSpeed(data.MoveSpeed);
        }
    }

    public void SaveData(IGameContext context)
    {
        var character = context.GetService<CharacterService>().GetCharacter();
        var data = new CharacterData
        {
            CurrentHipPoints = character.Get<IComponent_MaxHitPoints>().GetMaxHitPoints(),
            MaxHitPoints = character.Get<IComponent_HitPoints>().GetHitPoints(),
            MeleeDamage = character.Get<IComponent_MeleeDamage>().GetMeleeDamage(),
            MoveSpeed = character.Get<IComponent_MoveSpeed>().GetMoveSpeed()
        };
        
        _repository.SaveStats(data);
    }
}
