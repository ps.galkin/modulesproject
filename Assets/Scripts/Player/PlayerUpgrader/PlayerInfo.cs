﻿using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerInfo", menuName = "Lessons/New Product (Presentation Model)", order = 0)]
public class PlayerInfo : ScriptableObject
{
    [PreviewField] [SerializeField] public Sprite Icon;

    [SerializeField] public string Title;

    [TextArea] [SerializeField] public string Description;
}
