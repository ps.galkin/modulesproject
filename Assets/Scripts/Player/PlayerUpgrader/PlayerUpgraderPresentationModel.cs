﻿using System;
using UnityEngine;

public class PlayerUpgraderPresentationModel : PlayerPopup.IPresentationModel
{
    private readonly PlayerInfo _playerInfo;
    private readonly PlayerParams _playerParams;
    private readonly PlayerUpgrader _playerUpgrader;

    public PlayerUpgraderPresentationModel(PlayerInfo playerInfo, PlayerParams playerParams, PlayerUpgrader playerUpgrader)
    {
        _playerInfo = playerInfo;
        _playerParams = playerParams;
        _playerUpgrader = playerUpgrader;
    }

    public event Action OnPlayerParamsChanged;

    public void Start()
    {
        _playerParams.OnParamsChanged += OnParamsChanged;
    }

    public void Stop()
    {
        _playerParams.OnParamsChanged -= OnParamsChanged;
    }

    public string GetTitle() => _playerInfo.Title;
    
    public string GetDescription() => _playerInfo.Description;

    public Sprite GetImage() => _playerInfo.Icon;
    
    public string GetLevel() => $"Level: {_playerParams.Level}";

    public string GetHitPoints() => $"HitPoints: {_playerParams.HitPoints}";

    public string GetDamage() => $"Damage: {_playerParams.Damage}";

    public void OnBuyClicked()
    {
        _playerUpgrader.Upgrade();
    }

    private void OnParamsChanged()
    {
        OnPlayerParamsChanged?.Invoke();
    }
}