﻿using Components;
using GameElements;
using Sirenix.OdinInspector;
using UnityEngine;

public class PlayerUpgrader : MonoBehaviour, IGameInitElement
{
    private PlayerParams _playerParams;
    private IComponent_MoveSpeed _characterMoveSpeed;
    private IComponent_MeleeDamage _characterMeleeDamage;
    private IComponent_HitPoints _characterHitPoints;
        
    [Button]
    public void Upgrade()
    {
        _playerParams.Upgrade();
    }

    [Button]
    public void UpgradeSpeed(int value)
    {
        _characterMoveSpeed.SetMoveSpeed(value);
    }

    [Button]
    public void UpgradeDamage(int value)
    {
        _characterMeleeDamage.SetMeleeDamage(value);
    }

    [Button]
    public void UpgradeHitPoints(int value)
    {
        _characterHitPoints.SetHitPoints(value);
    }
    
    public void InitGame(IGameContext context)
    {
        _playerParams = context.GetService<PlayerParams>();

        var characterService = context.GetService<CharacterService>();
        _characterMoveSpeed = characterService.GetCharacter().Get<IComponent_MoveSpeed>();
        _characterMeleeDamage = characterService.GetCharacter().Get<IComponent_MeleeDamage>();
        _characterHitPoints= characterService.GetCharacter().Get<IComponent_HitPoints>();
    }
}
