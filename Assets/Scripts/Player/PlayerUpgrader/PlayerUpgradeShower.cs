﻿using GameElements;
using Sirenix.OdinInspector;
using UnityEngine;

public class PlayerUpgradeShower : MonoBehaviour, IGameInitElement
{
    private PopupManager _popupManager;
    private PlayerUpgrader _playerUpgrader;
    private PlayerParams _playerParams;

    [Button]
    public void ShowPlayerUpgrade(PlayerInfo playerInfo)
    {
        var presentationModel = new PlayerUpgraderPresentationModel(playerInfo,_playerParams, _playerUpgrader);
        _popupManager.ShowPopup(PopupName.PLAYER_UPGRADE, presentationModel);
    }

    public void InitGame(IGameContext context)
    {
        _popupManager = context.GetService<PopupManager>();
        _playerUpgrader = context.GetService<PlayerUpgrader>();
        _playerParams = context.GetService<PlayerParams>();
    }
}
