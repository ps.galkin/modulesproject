﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

public class PlayerParams : MonoBehaviour
{
    public event Action OnParamsChanged;
    
    public int Level => _level;
    public int HitPoints => _hitPoints;
    public int Damage => _damage;
    
    [Unity.Collections.ReadOnly] [ShowInInspector] private int _level;
    [Unity.Collections.ReadOnly] [ShowInInspector] private int _hitPoints;
    [Unity.Collections.ReadOnly] [ShowInInspector] private int _damage;

    public void Upgrade()
    {
        _level++;
        _hitPoints += 10;
        _damage += 2;
        
        OnParamsChanged?.Invoke();
    }
}
