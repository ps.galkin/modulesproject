public sealed class PopupArgs
{
    public readonly string Title;

    public readonly string Description;

    public PopupArgs(string title, string description)
    {
        Title = title;
        Description = description;
    }
}
