using TMPro;
using UnityEngine;

public sealed class PopupInfoView : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _titleText;

    [SerializeField]
    private TextMeshProUGUI _descriptionText;

    public void Show(object args)
    {
        if (args is PopupArgs infoArgs)
        {
            _titleText.text = infoArgs.Title;
            _descriptionText.text = infoArgs.Description;
        }
    }
}
