﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPopup : Popup
{
    [SerializeField] private TextMeshProUGUI _titleText;
    [SerializeField] private TextMeshProUGUI _descriptionText;
    [SerializeField] private Image _iconImage;
    [SerializeField] private TextMeshProUGUI _levelText;
    [SerializeField] private TextMeshProUGUI _hitPointsText;
    [SerializeField] private TextMeshProUGUI _damageText;
    [SerializeField] private TextButton _levelUpButton;

    private IPresentationModel _presenter;
    
    protected override void OnShow(object args)
    {
        if (args is not IPresentationModel presenter)
        {
            throw new Exception("Expected Presentation model!");
        }

        _presenter = presenter;
        _presenter.Start();

        _titleText.text = presenter.GetTitle();
        _descriptionText.text = presenter.GetDescription();
        _iconImage.sprite = presenter.GetImage();
        _levelText.text = presenter.GetLevel();
        _hitPointsText.text = presenter.GetHitPoints();
        _damageText.text = presenter.GetDamage();
        _presenter.OnPlayerParamsChanged += OnPlayerParamsChanged;
        
        _levelUpButton.AddListener(OnLevelUpButtonClicked);
    }
    
    protected override void OnHide()
    {
        _levelUpButton.RemoveListener(OnLevelUpButtonClicked);
        _presenter.OnPlayerParamsChanged -= OnPlayerParamsChanged;
        _presenter.Stop();
    }
    
    private void OnPlayerParamsChanged()
    {
        _levelText.text = _presenter.GetLevel();
        _hitPointsText.text = _presenter.GetHitPoints();
        _damageText.text = _presenter.GetDamage();
    }
    
    private void OnLevelUpButtonClicked()
    {
        _presenter.OnBuyClicked();
    }
    
    public interface IPresentationModel
    {
        event Action OnPlayerParamsChanged; 
        
        void Start();

        void Stop();

        string GetTitle();

        string GetDescription();

        Sprite GetImage();

        string GetLevel();

        string GetHitPoints();

        string GetDamage();
        
        void OnBuyClicked();
    }
}
