﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TextButton : MonoBehaviour
{
     
    [SerializeField] private Button _button;

    [Space]
    [SerializeField] private Image _buttonBackground;
    [SerializeField] private Sprite _availableButtonSprite;
    [SerializeField] private Sprite _lockedButtonSprite;

    [Space]
    [SerializeField] private TextMeshProUGUI _priceText;

    [Space]
    [SerializeField] private State _state;
    public void AddListener(UnityAction action)
    {
        _button.onClick.AddListener(action);
    }

    public void RemoveListener(UnityAction action)
    {
        _button.onClick.RemoveListener(action);
    }

    public void SetPrice(string price)
    {
        _priceText.text = price;
    }
    
    public void SetState(State state)
    {
        _state = state;

        if (state == State.AVAILABLE)
        {
            _button.interactable = true;
            _buttonBackground.sprite = _availableButtonSprite;
        }
        else if (state == State.LOCKED)
        {
            _button.interactable = false;
            _buttonBackground.sprite = _lockedButtonSprite;
        }
        else
        {
            throw new Exception($"Undefined button state {state}!");
        }
    }

    public enum State
    {
        AVAILABLE,
        LOCKED,
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        try
        {
            SetState(_state);
        }
        catch (Exception)
        {
            // ignored
        }
    }
#endif
}
